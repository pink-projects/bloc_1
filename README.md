# Traceur de Graphes avec Turtle

## Aperçu
Ce script Python exploite la bibliothèque `turtle` pour créer des visualisations graphiques à partir de données contenues dans des fichiers CSV. Principalement conçu pour afficher des données boursières, telles que celles d'Amazon (AMZN), il est suffisamment flexible pour être adapté à d'autres jeux de données.

## Fonctionnalités
- **Chargement dynamique des données :** Les données sont extraites de fichiers CSV configurés dans le script. Les chemins de ces fichiers peuvent être modifiés selon les besoins de l'utilisateur.
- **Personnalisation du graphique :** Le script permet de personnaliser l'apparence du graphique, y compris les couleurs et les échelles, grâce à des dictionnaires prédéfinis.
- **Visualisation interactive :** Utilisation des capacités de dessin de Turtle pour offrir une représentation visuelle interactive et attrayante des données.

## Prérequis
- **Python 3.x**
- **Module `turtle`** (inclus dans la plupart des installations de Python)
- **Module `csv`** pour la lecture des fichiers CSV
- **Fichiers CSV** contenant les données à visualiser

## Installation et exécution
1. **Préparer les fichiers de données :** Assurez-vous que les fichiers CSV sont formatés correctement et placés aux chemins indiqués dans le script.
2. **Modifier les chemins des fichiers (si nécessaire) :** Les chemins vers les fichiers CSV sont codés en dur dans le script. Mettez à jour ces chemins pour qu'ils correspondent aux emplacements réels sur votre système.
3. **Lancer le script :** Exécutez le script dans votre environnement Python. Une fenêtre graphique Turtle s'ouvrira, et les données seront tracées en fonction des fichiers CSV chargés.

## Utilisation
Utilisez la fonction `choice(path)`, où `path` est le chemin vers le fichier CSV contenant les données que vous souhaitez visualiser. Cette fonction configure le graphique, initialise le traçage, et affiche les données selon le fichier spécifié.

## Personnalisation
- **Dictionnaires de configuration :** Modifiez les dictionnaires `path_dico`, `scale_dico`, et `print_values_dico` pour ajuster le traitement et l'affichage des données pour différents fichiers.
- **Apparence du graphique :** Modifiez les paramètres de couleur et d'échelle dans les fonctions `initialize` et `draw_data` pour personnaliser l'apparence du graphique.

## Exemples de Graphiques
### Graphique généré avec `path_1`
![Graphique Path 1](path_1.png)

### Graphique généré avec `path_2`
![Graphique Path 2](path_2.png)

## Note
Ce script utilise des chemins absolus et des noms de fichiers spécifiques. Des ajustements seront nécessaires pour une utilisation avec d'autres fichiers ou sur différents systèmes.

---

Les images incluses montrent les graphiques résultants pour les deux configurations de chemin différentes, illustrant comment le script peut être utilisé pour visualiser divers ensembles de données financières.