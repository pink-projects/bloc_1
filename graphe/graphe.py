import turtle
import csv

# Initialisation des "tortues" pour le dessin
turt = turtle.Turtle()
turtX = turtle.Turtle()
turtY = turtle.Turtle()

# Configuration initiale de la taille de l'écran
size= turtle.screensize()
start_x= -size[0] + 50
AMZ_data=[]

# Chemins des fichiers CSV
path_1="C:/Users/Admin/Documents/projects/graphe/bloc_1/graphe/AMZN.csv"
path_2= "C:/Users/Admin/Documents/projects/graphe/bloc_1/graphe/AMZN_2.csv"

# Dictionnaires pour la configuration des fichiers
path_dico={path_1:2,path_2:1}
scale_dico={path_1:50,path_2:3}
print_values_dico ={path_1:3,path_2:40}


def read(path):
    """Lire les données à partir d'un fichier CSV et les stocker dans une liste."""
    with open(path) as f:
        reader = csv.reader(f)
        for i,j in enumerate(reader):
            if (i==0):
                continue
            value = j[path_dico[path]].replace(',', '.')
            AMZ_data.append(float(value))
        #print(AMZ_data)
            



def axis(title,speed):
    """Configurer les axes X et Y du graphique."""
    turtY.hideturtle()
    turtX.hideturtle()
    turtle.Screen().title(title)
    turtX.getscreen()
    turtX.speed(speed) 

    turtX.penup()
    turtX.goto(-size[0], 0)
    turtX.showturtle()
    turtX.pendown()

    turtX.forward(5*size[1])

    turtY.getscreen()
    turtY.speed(speed) 
    turtY.penup()
    turtY.left(90)
    turtY.goto(start_x, start_x)
    turtY.showturtle()
    turtY.pendown()

    turtY.forward(3*size[0])

def initialize(title, speed,y_scale, zoom_factor, color,data):
    turt.hideturtle()
    axis(title,speed)
    turt.showturtle()
    turt.speed(speed) 

    turt.penup()
    first_x = start_x
    first_y = data[0] * y_scale * zoom_factor
    turt.goto(first_x, first_y)
    turt.pendown()
    turt.color(color)
    turt.fillcolor('black') 

def draw_data(data,x_scale, y_scale, zoom_factor, graph_color, data_color,path):
    indexes= []
    for i in range(0,len(AMZ_data),print_values_dico[path]):
       indexes.append(i)
 
    #print(indexes)
    for i, value in enumerate(data):
        new_x = start_x + i*x_scale*zoom_factor
        new_y = value*y_scale*zoom_factor
        turt.goto(new_x, new_y)
        if (i in indexes):
            turt.color(data_color,"black")
            turt.write(value, align="right", font=("Verdana",12,"bold"))
            turt.dot(5)
            turt.color(graph_color)
            turt.fillcolor('black') 

def choice (path):

    x_factor=scale_dico[path]
    y_factor=1
    zoom=1
    read(path)
    initialize("Stock AMZ","fastest",y_factor,zoom,"blue",AMZ_data)
    draw_data(AMZ_data,x_factor,y_factor,zoom,"blue","red",path)


choice(path_2)
turtle.done()

